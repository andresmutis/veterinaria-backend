package com.vet.veterinaria.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.vet.veterinaria.dto.PetDto;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@AutoConfigureMockMvc
@Rollback
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
public class PetControllerTest {


    private static final Long ID = -1L;
    private static final String name = "Kira";
    private static final String race = " bull terry";
    private static final String species = "perro";
    private static final int age = 4;
    private static final String genere = "femenino";

    @Autowired
    protected MockMvc mvc;


    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return objectMapper.writeValueAsString(obj);
    }


    @Test
    @DisplayName("createTest")
    @Order(1)
    @Transactional(propagation = Propagation.REQUIRED)
    public void create_user_controller() throws Exception {
        PetDto pet = new PetDto();
        // pet.setId(1L);
        pet.setName(name);
        pet.setRace(race);
        pet.setSpecies(species);
        pet.setAge(age);
        pet.setGenere(genere);

        mvc.perform(MockMvcRequestBuilders.post(PetController.URL_CONTROLLER + "/create").
                contentType(MediaType.APPLICATION_JSON).content(mapToJson(pet)))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    @Order(2)
    @Transactional(propagation = Propagation.REQUIRED)
    public void update_user_controller() throws Exception {
        PetDto pet = new PetDto();
        pet.setId(ID);
        pet.setGenere("test");
        pet.setName(name);
        pet.setRace(race);
        pet.setSpecies(species);
        pet.setAge(age);
        mvc.perform(MockMvcRequestBuilders.put( PetController.URL_CONTROLLER +"/update").
                contentType(MediaType.APPLICATION_JSON).content(mapToJson(pet)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @Order(3)
    @Transactional(propagation = Propagation.REQUIRED)
    void finAll() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(PetController.URL_CONTROLLER + "/findPetAll"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    @Order(4)
    void findByIdTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(PetController.URL_CONTROLLER + "/findById/"+ID))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @Rollback
    @Order(5)
    @Sql(scripts = "/models/pet_test_data.sql", config = @SqlConfig(commentPrefix = "`"))
    void deleteByIdTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete(PetController.URL_CONTROLLER + "/deleteById/" + ID))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }










}
