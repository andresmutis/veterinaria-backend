package com.vet.veterinaria.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.vet.veterinaria.dto.UserDto;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@AutoConfigureMockMvc
@Rollback
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
public class UserControllerTest {
    private static final Long id = -1L;
    private static final String name = "Pepito";
    private static final String email = "pepito@gmail.com";
    private static final String cellphone = "323529";
    private static final int age = 23;
    private static final String direction = "Calle";

    @Autowired
    protected MockMvc mvc;

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return objectMapper.writeValueAsString(obj);
    }


    @Test
    @DisplayName("Test create user controller")
    @Order(1)
    @Transactional(propagation = Propagation.REQUIRED)
    public void create_user_controller() throws Exception {
        UserDto user = new UserDto();
        user.setName(name);
        user.setEmail(email);
        user.setCellphone(cellphone);
        user.setAge(age);
        user.setDirection(direction);

        mvc.perform(MockMvcRequestBuilders.post( UserController.URL_CONTROLLER +"/create").
                contentType(MediaType.APPLICATION_JSON).content(mapToJson(user)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @DisplayName("Test create user controller")
    @Order(2)
    @Transactional(propagation = Propagation.REQUIRED)
    public void update_user_controller() throws Exception {
        UserDto user = new UserDto();
        user.setId(id);
        user.setName(name);
        user.setEmail(email);
        user.setCellphone(cellphone);
        user.setAge(age);
        user.setDirection(direction);

        mvc.perform(MockMvcRequestBuilders.put( UserController.URL_CONTROLLER +"/update").
                contentType(MediaType.APPLICATION_JSON).content(mapToJson(user)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @DisplayName("Test find all users controller")
    @Order(3)
    @Transactional(propagation = Propagation.REQUIRED)
    void finAll() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(UserController.URL_CONTROLLER + "/findUserAll"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @DisplayName("Test find user by id controller")
    @Order(4)
    void findByIdTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(UserController.URL_CONTROLLER + "/findById/" + id))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @DisplayName("Test delete user by id controller")
    @Rollback
    @Order(5)
    @Sql(scripts = "/models/user_test_data.sql", config = @SqlConfig(commentPrefix = "`"))
    void deleteByIdTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete(UserController.URL_CONTROLLER + "/deleteById/" + id))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
