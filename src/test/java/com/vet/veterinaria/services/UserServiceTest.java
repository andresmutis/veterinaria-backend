package com.vet.veterinaria.services;

import com.vet.veterinaria.models.User;

import com.vet.veterinaria.repositories.UserRepository;
import com.vet.veterinaria.services.impl.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;

import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;


import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.doReturn;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @MockBean
    private UserRepository userRepository = Mockito.mock(UserRepository.class);

    @InjectMocks
    private UserService userService;

    @Test
    @DisplayName("Test create user")
    void should_saved_user_successfully(){
        // Configuracion del mock repository
        final User user = new User(null, "Daniel", "cdaniel@gmail.com", "3235291391", 23, "Calle 79");
        doReturn(user).when(userRepository).save(any());

        // Ejecución de la llamada al servicio
        User returnedUser = userService.create(user);

        // Asegurarse de la respuesta
        Assertions.assertNotNull(returnedUser, "Los campos del usuario no deben ser nulos");
    }

    @Test
    @DisplayName("Test find user by id")
    void should_return_user_by_id(){
        // Configuracion del mock repository
        User user = new User(7L, "Daniel", "cdaniel@hotmail.com","3235291391", 21, "Calle");
        doReturn(Optional.of(user)).when(userRepository).findById(7L);

        // Ejecución de la llamada al servicio
        Optional<User> returnedUser = userService.findById(7L);

        // Asegurarse de la respuesta
        Assertions.assertTrue(returnedUser.isPresent(), "Usuario no encontrado");
        Assertions.assertSame(returnedUser.get(), user, "El usuario encontrado no es el mismo que el del mock");
    }

    @Test
    @DisplayName("Test update user")
    void should_update_user_successfully(){

        final User user = new User(null, "Daniel", "cdaniel@gmail.com", "3235291391", 23, "Calle 79");
        doReturn(user).when(userRepository).save(any());

        // Ejecución de la llamada al servicio
        User returnedUser = userService.update(user);

        // Asegurarse de la respuesta
        Assertions.assertNotNull(returnedUser, "Los campos del usuario no deben ser nulos");
    }

    @Test
    @DisplayName("Test delete user")
    void should_delete_user_successfully(){

        User user = new User(4L, "Daniel", "cdaniel@hotmail.com","3235291391", 21, "Calle");
        doReturn(Optional.of(user)).when(userRepository).findById(7L);

        // Ejecución de la llamada al servicio
        userService.deleteById(user.getId());

        // Asegurarse de la respuesta
        verify(userRepository).deleteById(user.getId());
    }

    @Test
    @DisplayName("Test findAllUsers")
    void should_get_all_user(){

        List<User> user = userService.getAllUsers();

        Assertions.assertNotNull(user);

    }



}
