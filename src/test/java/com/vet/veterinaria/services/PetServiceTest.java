package com.vet.veterinaria.services;

import com.vet.veterinaria.models.Pet;
import com.vet.veterinaria.models.User;
import com.vet.veterinaria.repositories.PetRepository;
import com.vet.veterinaria.services.impl.PetService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;

import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;



import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.doReturn;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)

public class PetServiceTest {

    @MockBean
    private PetRepository petRepository = Mockito.mock(PetRepository.class);

    @InjectMocks
    private PetService petService;

    @Test
    @DisplayName("Test create pet")
    void should_saved_user_successfully(){
        // Configuracion del mock repository
        final Pet pet = new Pet(null, "Makoa", "caiman", "Tortuga", 128, "Macho");
        doReturn(pet).when(petRepository).save(any());

        // Ejecución de la llamada al servicio
        Pet returnedPet = petService.create(pet);

        // Asegurarse de la respuesta
        Assertions.assertNotNull(returnedPet, "Los campos de la mascota no deben ser nulos");
    }

    @Test
    @DisplayName("Test find pet by id")
    void should_return_pet_by_id(){

        // Configuracion del mock repository
        Pet pet = new Pet(1L, "drogoz", "draco","dragon", 20, "macho");
        doReturn(Optional.of(pet)).when(petRepository).findById(1L);

       //  Ejecución de la llamada al servicio
        Optional<Pet> returnedPet = petService.findById(1L);

        // Asegurarse de la respuesta
        Assertions.assertTrue(returnedPet.isPresent(), "Mascota no encontrado");
        Assertions.assertSame(returnedPet.get(), pet, "La Mascota encontrada no es el mismo que el del mock");
    }

    @Test
    @DisplayName("Test update pet")
    void should_update_pet_successfully(){

        final Pet pet = new Pet(null, "drogoz", "draco", "dragon", 21, "macho");
        doReturn(pet).when(petRepository).save(any());

        // Ejecución de la llamada al servicio
        Pet returnedPet = petService.update(pet);

        // Asegurarse de la respuesta
        Assertions.assertNotNull(returnedPet, "Los campos de la Mascota no deben ser nulos");
    }

    @Test
    @DisplayName("Test delete pet")
    void should_delete_pet_successfully(){

        Pet pet = new Pet(1L, "drogoz", "draco","dragon", 20, "macho");
        doReturn(Optional.of(pet)).when(petRepository).findById(1L);

        // Ejecución de la llamada al servicio
        petService.deleteById(pet.getId());

        // Asegurarse de la respuesta
        verify(petRepository).deleteById(pet.getId());
    }

    @Test
    @DisplayName("Test findAllPets")
    void should_get_all_pet(){

        List<Pet> widgets = petService.getAllPets();

        Assertions.assertNotNull(widgets);

    }

}
