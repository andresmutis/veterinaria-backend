package com.vet.veterinaria.exceptions;


import com.vet.veterinaria.dto.SimpleObjectResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import java.io.FileNotFoundException;
import java.sql.SQLException;

@ControllerAdvice
public class ApiExceptionHandler {

	/*@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({NotFoundException.class, FileNotFoundException.class})
	@ResponseBody
	public SimpleObjectResponse notFoundRequest(HttpServletRequest request, Exception exception) {
		return new SimpleObjectResponse(HttpStatus.NOT_FOUND.value(), exception.getMessage(), request.getRequestURI());
	}*/


	/*@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({BadRequestException.class, org.springframework.dao.DuplicateKeyException.class,
			org.springframework.dao.DataAccessException.class, SQLException.class,
			org.springframework.dao.EmptyResultDataAccessException.class,
			org.springframework.web.HttpRequestMethodNotSupportedException.class,
			MethodArgumentNotValidException.class,
			org.springframework.web.bind.MissingRequestHeaderException.class,
			org.springframework.web.bind.MissingServletRequestParameterException.class,
			org.springframework.web.method.annotation.MethodArgumentTypeMismatchException.class,
			org.springframework.http.converter.HttpMessageNotReadableException.class, MethodArgumentNotValidException.class,
			NullPointerException.class
	})*/

	/*@ResponseBody
	public SimpleObjectResponse badRequest(HttpServletRequest request, Exception exception) {
		String message = exception.getMessage();
		StringBuilder errors= new StringBuilder();
		//Controla excepciones de un objeto desde el controller
		if (exception instanceof MethodArgumentNotValidException) {
			((MethodArgumentNotValidException) exception).getBindingResult().getAllErrors().forEach(error -> {
				String errorMessage = error.getDefaultMessage();
				errors.append(errorMessage);
			});
			message=errors.toString();
		}
		//Controla excepciones de un objeto desde el service
		if (exception instanceof javax.validation.ConstraintViolationException) {
			for (ConstraintViolation<?> violation : ((javax.validation.ConstraintViolationException) exception).getConstraintViolations()) {
				String queryParamPath = violation.getPropertyPath().toString();
				String field = queryParamPath;
				if (Boolean.TRUE.equals(queryParamPath.contains("."))){
					String[] parts = queryParamPath.split("\\.");
					field = parts[parts.length-1];
				}
				errors.append(String.format("%s : %s.", field, violation.getMessage()));
			}
			message=errors.toString();
		}

		return new SimpleObjectResponse(HttpStatus.BAD_REQUEST.value(), message,
				request.getRequestURI());
	}*/

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({ Exception.class , ServletException.class})
	@ResponseBody
	public SimpleObjectResponse fatalErrorUnexpectedException(HttpServletRequest request, Exception exception) {
		return new SimpleObjectResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.toString(), request.getRequestURI());
	}


	/*@ResponseStatus(HttpStatus.CREATED)
	@ExceptionHandler({ ObjectRequestException.class })
	@ResponseBody
	public SimpleObjectResponse objectRequest(HttpServletRequest request, Exception exception) {
		return new SimpleObjectResponse(HttpStatus.CREATED.value(), exception.getMessage(), request.getRequestURI());
	}*/





}
