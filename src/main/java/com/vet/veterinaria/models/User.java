package com.vet.veterinaria.models;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name= "tb_users")
@ApiModel(value = "Usuarios - Model", description = "Entidad para administrar la información de los usuarios")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="us_id")
    private Long id;

    @NotBlank(message = "El campos name no debe ser nulo")
    @Column(name="us_name")
    private String name;

    @NotBlank(message = "El campos email no debe ser nulo")
    @Column(name="us_email")
    private String email;

    @NotBlank(message = "El campos cellphone no debe ser nulo")
    @Column(name="us_cellphone")
    private String cellphone;

    @NotNull(message = "El campos age no debe ser nulo")
    @Column(name="us_age")
    private int age;

    @NotBlank(message = "El campos us_direction no debe ser nulo")
    @Column(name="us_direction")
    private String direction;

}
