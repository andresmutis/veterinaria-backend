package com.vet.veterinaria.models;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name= "tb_pets")
@ApiModel(value = "Mascotas - Model", description = "Entidad para administrar la información de las mascotas")
public class Pet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="pet_id")
    private Long id;

    @NotBlank(message = "El campo pet_name no debe ser nulo")
    @Column(name="pet_name")
    private String name;

    @NotBlank(message = "El campo pet_race no debe ser nulo")
    @Column(name="pet_race")
    private String race;

    @NotBlank(message = "El campo pet_specie no debe ser nulo")
    @Column(name="pet_species")
    private String species;

    @NotNull(message = "El campo pet_age no debe ser nulo")
    @Column(name="pet_age")
    private int age;

    @NotBlank(message = "El campo pet_genere no debe ser nulo")
    @Column(name="pet_genere")
    private String genere;

}