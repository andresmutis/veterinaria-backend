package com.vet.veterinaria.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class UserDto {


    private Long id;
    private String name;
    private String email;
    private String cellphone;
    private int age;
    private String direction;

}
