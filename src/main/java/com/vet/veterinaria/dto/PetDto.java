package com.vet.veterinaria.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PetDto {


    private Long id;
    private String name;
    private String race;
    private String species;
    private int age;
    private String genere;

}