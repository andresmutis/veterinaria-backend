package com.vet.veterinaria.controllers;

import com.vet.veterinaria.dto.PetDto;
import com.vet.veterinaria.dto.SimpleObjectResponse;
import com.vet.veterinaria.models.Pet;
import com.vet.veterinaria.services.impl.PetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.modelmapper.ModelMapper;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Api(tags = "Pet Type API")
@RequestMapping("/pet")
@CrossOrigin
@Validated
public class PetController {


    public static final String URL_CONTROLLER = "/pet";
    private final PetService petService;
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public PetController(PetService petService) {
        this.petService = petService;
    }

    @Bean
    public ModelMapper petModelMapper() {
        return modelMapper;
    }

    @ApiOperation(nickname = "create", notes = "Este método crea una mascota.", value = "Crear mascota", response = Pet.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mascota creada con éxito", response = Pet.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = Pet.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = Pet.class)})
    @PostMapping(value = "/create", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> create(@NotNull(message = "PetDto no puede ser nulo.") @Valid @RequestBody PetDto petDto) {
        petService.create(modelMapper.map(petDto, Pet.class));
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Mascota creada con éxito").build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "update", notes = "Este método actualiza una mascota.", value = "Actualizar mascota", response = Pet.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mascota actualizada con éxito", response = Pet.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = Pet.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = Pet.class)})
    @PutMapping(value = "/update", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> update(@NotNull(message = "PetDto no puede ser nulo.") @Valid @RequestBody PetDto petDto) {
        petService.update(modelMapper.map(petDto, Pet.class));
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Mascota actualizada con éxito").build(),
                HttpStatus.OK);
    }


    @ApiOperation(nickname = "findall", notes = "Este método lista todos las mascotas.", value = "Listar mascotas", response = Pet.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Listadoa de mascotas con éxito", response = Pet.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = Pet.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = Pet.class)})
    @GetMapping(value = "/findPetAll", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findAll() {
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Listado de mascotas con éxito").value(petService.getAllPets()).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "findbyid", notes = "Este método lista a la mascota mediante la búsqueda con su id.", value = "Buscar mascota", response = Pet.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Encontrado con éxito", response = Pet.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = Pet.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = Pet.class)})
    @GetMapping(value = "/findById/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findById(@PathVariable Long id) {
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Encontrado con éxito").value(petService.findById(id)).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "deletebyid", notes = "Este método elimina a la mascota mediante su id.", value = "Eliminar mascota", response = Pet.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Eliminado con éxito", response = Pet.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = Pet.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = Pet.class)})
    @DeleteMapping(value = "/deleteById/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> deleteById(@PathVariable Long id) {
        petService.deleteById(id);
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Eliminado con éxito").value(null).build(),
                HttpStatus.OK);
    }


}