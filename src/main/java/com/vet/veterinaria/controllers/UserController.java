package com.vet.veterinaria.controllers;

import com.vet.veterinaria.dto.SimpleObjectResponse;
import com.vet.veterinaria.dto.UserDto;
import com.vet.veterinaria.models.User;
import com.vet.veterinaria.services.impl.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.modelmapper.ModelMapper;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Api(tags = "User Type API")
@RequestMapping("/user")
@CrossOrigin
@Validated
public class UserController {

    public static final String URL_CONTROLLER = "/user";
    private final UserService userService;
    ModelMapper modelMapper = new ModelMapper();


    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Bean
    public ModelMapper modelMapper() {
        return modelMapper;
    }

    @ApiOperation(nickname = "create", notes = "Este método crea un usuario.", value = "Crear usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuario creado con éxito", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @PostMapping(value = "/create", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> create(@NotNull(message = "UserDto no puede ser nulo.") @Valid @RequestBody UserDto userDto) {
        userService.create(modelMapper.map(userDto, User.class));
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Usuario creado con éxito").build(),
                    HttpStatus.OK);
    }

    @ApiOperation(nickname = "update", notes = "Este método actualiza un usuario.", value = "Actualizar usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuario actualizado con éxito", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @PutMapping(value = "/update", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> update(@NotNull(message = "UserDto no puede ser nulo.") @Valid @RequestBody UserDto userDto) {
        userService.update(modelMapper.map(userDto, User.class));
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Usuario actualizado con éxito").build(),
                HttpStatus.OK);
    }


    @ApiOperation(nickname = "findall", notes = "Este método lista todos los usuarios.", value = "Listar Usuarios", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Listados de usuarios con éxito", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @GetMapping(value = "/findUserAll", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findAll() {
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Listados de usuarios con éxito").value(userService.getAllUsers()).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "findbyid", notes = "Este método lista al usuario mediante la búsqueda con su id.", value = "Buscar Usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Encontrado con éxito", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @GetMapping(value = "/findById/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findById(@PathVariable Long id) {
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Encontrado con éxito").value(userService.findById(id)).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "deletebyid", notes = "Este método elimina al usuario mediante su id.", value = "Eliminar Usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Eliminado con éxito", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @DeleteMapping(value = "/deleteById/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> deleteById(@PathVariable Long id) {
        userService.deleteById(id);
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Eliminado con éxito").value(null).build(),
                HttpStatus.OK);
    }


}
